# Calculator

A decent calculator that performs all the arithmetical operations of a normal calculator. Can be used to study various basic coding concepts by beginners. Provides addition subtraction multiplication and division functionality in C++.